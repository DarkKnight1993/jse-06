package ru.tsc.goloshchapov.tm;

import ru.tsc.goloshchapov.tm.constant.TerminalConst;

import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        showWelcome();
        parseArgs(args);
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            showEnterCommand();
            final String command = scanner.nextLine();
            parseArg(command);
        }
    }

    public static void parseArg(final String arg) {
        if (TerminalConst.ABOUT.equals(arg)) showAbout();
        if (TerminalConst.VERSION.equals(arg)) showVersion();
        if (TerminalConst.HELP.equals(arg)) showHelp();
        if (TerminalConst.EXIT.equals(arg)) exit();
    }

    public static void parseArgs(String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        parseArg(arg);
        exit();
    }

    public static void exit() {
        System.exit(0);
    }

    public static void showWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    public static void showEnterCommand() {
        System.out.println("ENTER COMMAND:");
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("DEVELOPER: Vladimir Goloshchapov");
        System.out.println("E-MAIL: goloschapov@tsconsulting.com");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.println(TerminalConst.ABOUT + " - Display developer info");
        System.out.println(TerminalConst.VERSION + " - Display program version");
        System.out.println(TerminalConst.HELP + " - Display list of commands");
        System.out.println(TerminalConst.EXIT + " - Close application");
    }

}
